package uz.pdp.uyborproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UyborProjectApplication {

    public static void main(String[] args) {
        SpringApplication.run(UyborProjectApplication.class, args);
    }

}
